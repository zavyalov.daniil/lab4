﻿using System.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DynDataStructures
{
    public record Student(string name, double group,string exam,double mark);
    
    public class Class1
    {
        public static IEnumerable<string> HighestAverageScoreStudents (List<Student> examinationSheet){
            var rez = examinationSheet.GroupBy(
                student => student.name,
                student => student.mark,
                (studentName, studentScores) => new {
                    Key = studentName,
                    AverageScore = Math.Round(studentScores.Average(), 1)
                }).GroupBy(
                    student => student.AverageScore,
                    student => student.Key,
                    (AverageScore, Names) => new {
                        Key = AverageScore,
                        NamesArr = Names
                    }
                ).OrderByDescending (student => student.Key).First().NamesArr.OrderBy(student => student);
                //.Aggregate((max, student)=> max.Key < student.Key ? student : max).NamesArr.OrderBy(student => student);
            return rez;
        }

        public static Dictionary<string, double> AverageScoreSubjects (List<Student> examinationSheet) {
            var rez = examinationSheet.GroupBy(
                record =>record.exam,
                record =>record.mark,
                (Subject, Scores) => new {
                    Key = Subject,
                    AverageScore=Math.Round(Scores.Average(), 1)
                }
            ).OrderByDescending(subject => subject.Key).ToDictionary(subject => subject.Key, subject => subject.AverageScore);
            return rez;
        }
        public static Dictionary<string, double> SubjectHighestScoreGroup (List<Student> examinationSheet) {
            var rez = examinationSheet.GroupBy(
                record => record.exam,
                (subject, records) => new
                {
                    Key = subject,
                    SubjectRecords = records
                }
            ).Select(
                record => new {
                    Subject=record.Key,
                    HighestScoreGroup=record.SubjectRecords.GroupBy(
                        subjectRecords => subjectRecords.group,
                        subjectRecords => subjectRecords.mark,
                        (groupNumber, scores) => new {
                            Key = groupNumber,
                            AverageScore = Math.Round(scores.Average(), 1)
                        }
                    ).OrderByDescending(record => record.AverageScore).First().Key
                }
            ).ToDictionary(record => record.Subject, record => record.HighestScoreGroup);
            /*var rez = examinationSheet.GroupBy(
                record => record.group
            ).Select(record => new {Group=record.Key, Avg=record.Average(s => s.mark)});*/
            return rez;
        }
    }
}
