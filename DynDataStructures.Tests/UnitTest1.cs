using System.Text.RegularExpressions;
using System;
using Xunit;
using System.Collections.Generic;
using DynDataStructures;

namespace DynDataStructures.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void OneStudentHighestAverageScore()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel",1,"grammar",5),
                new Student("Munjid al-Kaleel",1,"rhetoric",4),
                new Student("Munjid al-Kaleel",1,"history",2), 
                new Student("Arhab el-Tahir",2,"history",5), 
                new Student("Arhab el-Tahir",2,"rhetoric",2), 
                new Student("Husain el-Mahdi",3,"rhetoric",1), 
                new Student("Abdul Majeed bin Abdulaziz",3,"rhetoric",2), 
                new Student("Fahda bint Asi Al Shuraim",2,"history", 3),
                new Student("Fahda bint Asi Al Shuraim",3,"grammar",2),
                new Student("Fahda bint Asi Al Shuraim",3,"rhetoric",5),
                new Student("Fahda bint Asi Al Shuraim",3,"history",5),
                new Student("Hussa bint Ahmed Al Sudairi",1,"grammar",3),
                new Student("Hussa bint Ahmed Al Sudairi",1,"rhetoric",3),
                new Student("Hussa bint Ahmed Al Sudairi",1,"history",3),
                new Student("Mishaari al-Koroma",1,"grammar",2),
                new Student("Mishaari al-Koroma",1,"rhetoric",2),
                new Student("Mishaari al-Koroma",1,"history",2)
            };
            Assert.Equal(new string[] {"Fahda bint Asi Al Shuraim"}, Class1.HighestAverageScoreStudents(table));
        }
        [Fact]
        public void TwoStudentsHighestAverageScore()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel",1,"grammar",4),
                new Student("Munjid al-Kaleel",1,"rhetoric",4),
                new Student("Munjid al-Kaleel",1,"history",4), 
                new Student("Arhab el-Tahir",2,"history",5), 
                new Student("Arhab el-Tahir",2,"rhetoric",2), 
                new Student("Husain el-Mahdi",3,"rhetoric",1), 
                new Student("Fahda bint Asi Al Shuraim",3,"grammar",5),
                new Student("Abdul Majeed bin Abdulaziz",3,"rhetoric",2), 
                new Student("Abdul Majeed bin Abdulaziz",2,"history", 3),
                new Student("Fahda bint Asi Al Shuraim",3,"rhetoric",5),
                new Student("Fahda bint Asi Al Shuraim",3,"history",2),
                new Student("Hussa bint Ahmed Al Sudairi",1,"grammar",3),
                new Student("Hussa bint Ahmed Al Sudairi",1,"rhetoric",3),
                new Student("Hussa bint Ahmed Al Sudairi",1,"history",3),
                new Student("Mishaari al-Koroma",1,"grammar",2),
                new Student("Mishaari al-Koroma",1,"rhetoric",2),
                new Student("Mishaari al-Koroma",1,"history",2)
            };
            Assert.Equal(new List<string> {"Fahda bint Asi Al Shuraim","Munjid al-Kaleel"}, Class1.HighestAverageScoreStudents(table));
        }

        [Fact]
        public void AverageScoreSubjectsTest ()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel", 1 ,"grammar", 4),
                new Student("Munjid al-Kaleel", 1 ,"rhetoric", 4),
                new Student("Munjid al-Kaleel", 1 ,"history", 4), 
                new Student("Arhab el-Tahir", 2 ,"history", 5), 
                new Student("Arhab el-Tahir", 2 ,"rhetoric", 2), 
                new Student("Husain el-Mahdi", 3 ,"rhetoric", 1), 
                new Student("Fahda bint Asi Al Shuraim", 3 ,"grammar", 5),
                new Student("Abdul Majeed bin Abdulaziz", 3 ,"rhetoric", 2), 
                new Student("Abdul Majeed bin Abdulaziz", 3 ,"history", 3),
                new Student("Fahda bint Asi Al Shuraim", 3 ,"rhetoric", 5),
                new Student("Fahda bint Asi Al Shuraim", 3 ,"history", 2),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"grammar", 3),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"rhetoric", 3),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"history", 3),
                new Student("Mishaari al-Koroma", 1 ,"grammar", 2),
                new Student("Mishaari al-Koroma", 1 ,"rhetoric", 2),
                new Student("Mishaari al-Koroma", 1 ,"history", 2)
            };
            Assert.Equal(new Dictionary<string, double> {{"grammar", 3.5}, {"history", 3.2}, {"rhetoric", 2.7}}, Class1.AverageScoreSubjects(table));
        }
        [Fact]
        public void AverageScoreSubjectsTest2 ()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel", 1 ,"history", 4), 
                new Student("Arhab el-Tahir", 2 ,"history", 5), 
                new Student("Abdul Majeed bin Abdulaziz", 3 ,"history", 3),
                new Student("Fahda bint Asi Al Shuraim", 3 ,"history", 2),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"history", 3),
                new Student("Mishaari al-Koroma", 1 ,"history", 2),
                new Student("Mishaari al-Koroma", 1 ,"grammar", 2)
            };
            Assert.Equal(new Dictionary<string, double> {{"grammar", 2}, {"history", 3.2}}, Class1.AverageScoreSubjects(table));
        }

        [Fact]
        public void AverageScoreSubjectsTestOneRecord ()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel", 1 ,"history", 4), 
            };
            Assert.Equal(new Dictionary<string, double> {{"history", 4}}, Class1.AverageScoreSubjects(table));
        }

        [Fact]
        public void SubjectHighestScoreGroupTest ()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel", 1 ,"grammar", 4),
                new Student("Munjid al-Kaleel", 1 ,"rhetoric", 4),
                new Student("Munjid al-Kaleel", 1 ,"history", 4), 
                new Student("Arhab el-Tahir", 2 ,"history", 5), 
                new Student("Arhab el-Tahir", 2 ,"rhetoric", 2), 
                new Student("Husain el-Mahdi", 3 ,"rhetoric", 1), 
                new Student("Fahda bint Asi Al Shuraim", 3 ,"grammar", 5),
                new Student("Abdul Majeed bin Abdulaziz", 3 ,"rhetoric", 2), 
                new Student("Abdul Majeed bin Abdulaziz", 3 ,"history", 3),
                new Student("Fahda bint Asi Al Shuraim", 3 ,"rhetoric", 5),
                new Student("Fahda bint Asi Al Shuraim", 3 ,"history", 2),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"grammar", 3),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"rhetoric", 3),
                new Student("Hussa bint Ahmed Al Sudairi", 1 ,"history", 3),
                new Student("Mishaari al-Koroma", 1 ,"grammar", 2),
                new Student("Mishaari al-Koroma", 1 ,"rhetoric", 2),
                new Student("Mishaari al-Koroma", 1 ,"history", 2)
            };
            Assert.Equal(new Dictionary<string, double> {{"grammar", 3}, {"history", 2}, {"rhetoric", 1}}, Class1.SubjectHighestScoreGroup(table));
            //1: grammar 3, history 3, rhetoric 3
            //2: grammar -, history 5, rhetoric 2
            //3: grammar 5, history 2.5, rhetoric 2.6
        }

        [Fact]
        public void SubjectHighestScoreOneGroup ()
        {
            List<Student> table = new List<Student> { 
                new Student("Munjid al-Kaleel", 2 ,"grammar", 4),
                new Student("Munjid al-Kaleel", 2 ,"rhetoric", 4),
                new Student("Munjid al-Kaleel", 2 ,"history", 4), 
                new Student("Arhab el-Tahir", 2 ,"history", 5), 
                new Student("Arhab el-Tahir", 2 ,"rhetoric", 2), 
                new Student("Husain el-Mahdi", 2 ,"rhetoric", 1), 
            };
            Assert.Equal(new Dictionary<string, double> {{"grammar", 2}, {"history", 2}, {"rhetoric", 2}}, Class1.SubjectHighestScoreGroup(table));
        }
    }
}
